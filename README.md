# finance

## Introduction
    * System of control finance with django with postresql.

## Prerequisites

Before you begin, make sure you have the following installed on your system:

    Python (version 3.6 or higher)
    pip (Python package installer)


## Installation

Clone the project repository:

```bash
git clone https://gitlab.com/alan.tavares1/finance.git
```

Navigate to the project directory:

```bash
cd finance/core
```

## Create a virtual environment:

On Windows:

```bash
python -m venv .venv
```

On macOS and Linux:

```bash
python3 -m venv .venv
```

## Activate the virtual environment:

On macOS and Linux:

```bash
source .venv/bin/activate
```

On Windows:

```bash
.venv/Scripts/activate
```

## Install project dependencies:

```bash
pip install -r requirements.txt
```

## .env file

> For default we have some pre-definitions that is our default configurations for this project, but you can change it creating a .env file with the command bellow

```bash
touch .env
```

and then add some variables

| Key  | Value       |
|------------|-------------|
| DB_NAME    | finance     |
| USER       | postgres    |
| PASSWORD   | master       |
| HOST       | localhost   |
| PORT       | 5432        |


## Set up the database:

```bash
python manage.py migrate
```

## Create a superuser (admin) account:

```bash
python manage.py createsuperuser
```

## Running the Project

To run the Django project, execute the following command:

```bash
python manage.py runserver
```

And you have the website active in localhost:8000 for test and use it.

Fell free to colaborate in this project!!!
