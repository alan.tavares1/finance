from datetime import date
from typing import Optional

from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required
from django.db.models import QuerySet
from django.shortcuts import redirect, render

from finance.models import Moviment, Type, User, Wallet
from logs.models import Log

from django.contrib import messages

def login_index(request):
    title: str = "Login - Fleeca"
    message_form: str = ""
    if request.user.is_authenticated:
        return redirect("/finance/home")

    if request.method == "POST":
        username: str = request.POST.get("username") or ""
        password: str = request.POST.get("password") or ""
        if not username:
            message_form = f"{message_form} - Username empty"
        if not password:
            message_form = f"{message_form} - Password empty"

        user: Optional[User] = authenticate(
            request, username=username, password=password
        )
        if user:
            login(request, user)
            return redirect("/finance/home")
        else:
            message_form = (
                f"{message_form} - Error ! Login or Password invalid!"
            )
    return render(request, "finance/login.html")


def logout_index(request):
    logout(request)
    messages.success(request,f'You have been logged out.')
    return redirect('/finance/')    

@login_required
def index(request):
    title: str = "Wallets"
    wallets: QuerySet[Wallet] = Wallet.objects.filter(user= request.user.id)
    return render(request, "finance/wallet/index.html", locals())


@login_required
def index_type(request):
    title: str = "Types"
    types: QuerySet[Type] = Type.objects.filter(user= request.user.id)
    return render(request, "finance/type/index.html", locals())


@login_required
def add_type(request):
    title: str = "Add type"
    message_form: str = ""
    if request.method == "POST":
        name: str = request.POST.get("name", "")
        operation: str = request.POST.get("operation", "")
        user = request.user
        if name and operation:
            type: Type = Type(name=name, operation=operation, user=user)
            type.save()
            Log.add(
                description=f"Add type of created with name:{name} - operation {operation}",
                app="finance",
                model="type",
                object_id=type.id,
                user=user.username,
            )
            return redirect("/finance/type")
        else:
            message_form = "ERROR! NAME OR OPERATION EMPTY!"
    return render(request, "finance/type/add.html", locals())


@login_required
def edit_type(request, type_id):
    title: str = "Edit type"
    message_form: str = ""
    type: Optional[Type] = Type.objects.filter(id=type_id).first()
    if not type:
        return redirect("/finance/type")
    if request.method == "POST":
        name: str = request.POST.get("name", "")
        operation: str = request.POST.get("operation", "")
        user = request.user
        if name and operation:
            type.name = name
            type.operation = operation
            type.save()
            Log.add(
                description=f"Type of update to with name:{name} - operation {operation}",
                app="finance",
                model="type",
                object_id=type.id,
                user=user.username,
            )
            return redirect("/finance/type")
        else:
            message_form = "ERROR! NAME OR OPERATION EMPTY!"
    return render(request, "finance/type/add.html", locals())


@login_required
def delete_type(request, type_id):
    item: Optional[Type] = Type.objects.filter(pk=type_id).first()
    if item:
        item.delete()
    return redirect("/finance/type/")


@login_required
def view_wallet(request, wallet_id):
    wallet: Optional[Wallet] = Wallet.objects.filter(id=wallet_id).first()
    if not wallet:
        return redirect("/finance/type/")
    title: str = wallet.name
    moviments: QuerySet[Moviment] = Moviment.objects.filter(wallet=wallet_id)
    return render(request, "finance/wallet/view_wallet.html", locals())


@login_required
def add_moviment_walet(request, wallet_id):
    wallet: Optional[Wallet] = Wallet.objects.filter(id=wallet_id).first()
    types: QuerySet[Type] = Type.objects.all()
    if not wallet:
        return redirect("/finance/type/")
    title: str = f"Add moviment in {wallet.name}"
    if request.method == "POST":
        name: str = request.POST.get("name", "")
        value: float = float(request.POST.get("value") or 0)
        type: Optional[Type] = Type.objects.filter(
            id=request.POST.get("type")
        ).first()
        user = User.objects.get_or_create(id=1)
        if name and type:
            moviment: Moviment = Moviment(
                name=name, value=value, type=type, wallet=wallet
            )
            moviment.save()
            moviment.calculate()
            user = request.user
            Log.add(
                description=f"Add moviment in wallet {wallet.name} of created with name:{name} - type {type.name} - value {value}",
                app="finance",
                model="moviment",
                object_id=moviment.id,
                user=user.username,
            )

            return redirect(f"/finance/wallet/{wallet.id}")
        else:
            message_form = "ERROR! NOT FOUND TYPE OR NAME in ADD Moviment!"
    return render(request, "finance/moviment/add.html", locals())


@login_required
def delete_moviment_walet(request, moviment_id):
    moviment: Optional[Moviment] = Moviment.objects.filter(
        id=moviment_id
    ).first()
    if moviment:
        wallet: Wallet = moviment.wallet
        balance: float = wallet.balance
        if moviment.type.operation == "+":
            balance -= moviment.value
        if moviment.type.operation == "-":
            balance += moviment.value
        wallet.balance = balance
        wallet.save()
        moviment.delete()
    return redirect(f"/finance/wallet/{wallet.id}")


@login_required
def edit_moviment_walet(request, moviment_id):
    return


@login_required
def add_wallet(request):
    title: str = "Add Wallet"
    message_form: str = ""
    if request.method == "POST":
        name: str = request.POST.get("name", "")
        balance: float = float(request.POST.get("balance") or 0)
        user = request.user
        if name:
            wallet: Wallet = Wallet(
                name=name,
                balance=balance,
                user=user,
                creation_date=date.today(),
            )
            wallet.save()
            Log.add(
                description=f"Add wallet of created with name:{name} - balance {balance}",
                app="finance",
                model="wallet",
                object_id=wallet.id,
                user=user.username,
            )
            return redirect("/finance/")
        else:
            message_form = "ERROR! NAME OR Wallet EMPTY!"
    return render(request, "finance/wallet/add.html", locals())


@login_required
def edit_wallet(request, wallet_id: int):
    title: str = "Edit Wallet"
    message_form: str = ""
    wallet: Optional[Wallet] = Wallet.objects.filter(id=wallet_id).first()
    if not wallet:
        return redirect("/finance/type")
    if request.method == "POST":
        name: str = request.POST.get("name", "")
        balance: float = float(request.POST.get("balance") or 0)
        user = request.user
        if name:
            wallet.name = name
            wallet.balance = balance
            wallet.save()
            Log.add(
                description=f"Wallet of update to with name:{name} - balance {balance}",
                app="finance",
                model="wallet",
                object_id=wallet.id,
                user=user.username,
            )
            return redirect("/finance/")
        else:
            message_form = "ERROR! NAME OR OPERATION EMPTY!"
    return render(request, "finance/wallet/add.html", locals())


@login_required
def delete_wallet(request, wallet_id: int):
    item: Optional[Wallet] = Wallet.objects.filter(pk=wallet_id).first()
    if item:
        user = request.user
        Log.add(
            description=f"Wallet of delete to with name:{item.name} - balance {item.balance}",
            app="finance",
            model="wallet",
            object_id=item.id,
            user=user.username,
        )
        item.delete()
    return redirect("/finance/")
