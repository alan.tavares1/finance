from django.contrib import admin

from finance.models import Type, Wallet, Moviment


class TypeAdmin(admin.ModelAdmin):
    search_fields = (
        "name",
        "operation",
    )
    list_display = (
        "id",
        "name",
        "operation",
    )
    list_display_icons = True
    list_filter = []
    fields = ["name", "operation", "user"]


class WalletAdmin(admin.ModelAdmin):
    search_fields = "name", "creation_date", "balance"
    list_display = "id", "name", "balance", "user"
    list_display_icons = True
    list_filter = []
    fields = ["name", "balance", "user", "created", "creation_date"]


class MovimentAdmin(admin.ModelAdmin):
    search_fields = (
        "name",
        "creation_date",
        "value",
    )
    list_display = "id", "name", "value", "type", "wallet"
    list_display_icons = True
    list_filter = []
    fields = ["name", "value", "type", "wallet"]


admin.site.register(Type, TypeAdmin)
admin.site.register(Wallet, WalletAdmin)
admin.site.register(Moviment, MovimentAdmin)
