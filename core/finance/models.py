from django.contrib.auth.models import AbstractBaseUser, UserManager
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

class User(AbstractBaseUser):
    username = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    name = models.TextField()
    is_active = models.BooleanField(default=True)
    objects = UserManager()
    created = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.email


class Type(models.Model):
    OPERATION_CHOICES = [("+", "Sum"), ("-", "Subtract")]
    name = models.TextField()
    operation = models.CharField(max_length=1, choices=OPERATION_CHOICES)

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.name


class Wallet(models.Model):
    name = models.TextField()
    creation_date = models.DateTimeField()
    created = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    balance = models.FloatField(default=0)

    def __str__(self) -> str:
        return self.name


class Moviment(models.Model):
    name = models.TextField()
    type = models.ForeignKey(Type, on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now)
    value = models.FloatField()
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.name

    def calculate(self):
        balance: float = self.wallet.balance
        if self.type.operation == "+":
            balance += self.value
        if self.type.operation == "-":
            balance -= self.value
        wallet: Wallet = self.wallet
        wallet.balance = balance
        wallet.save()
